package ru.helmetoff.tm;

import ru.helmetoff.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        displayWelcome();
        if (parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            parseArg(command);
        }
    }

    private static void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.CMD_HELP:
                displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
            default:
                displayWrongCommand(arg);
                break;
        }
    }

    private static boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void exit() {
        System.exit(0);
    }

    private static void displayWelcome() {
        System.out.println(TerminalConst.WELCOME_MESSAGE);
    }

    private static void displayHelp() {
        System.out.println(TerminalConst.HELP_MESSAGE);
    }

    private static void displayVersion() {
        System.out.println(TerminalConst.VERSION_MESSAGE);
    }

    private static void displayAbout() {
        System.out.println(TerminalConst.ABOUT_MESSAGE);
    }

    private static void displayWrongCommand(final String arg) {
        System.out.println("'" + arg + "'" + TerminalConst.WRONG_COMMAND_MESSAGE);
    }

}
